local start_point_x = 100
local start_point_y = -100
local button_width = 150
local button_height = 30
local step_y = 25
local step_x = 150
local max_y = 600

local api_descriptions = {
	{
		name = "ActionButtonDown",
		callback = function() 
			ActionButtonDown(2)
		end
	},
	{
		name = "ActionButtonUp",
		callback = function()
			ActionButtonUp(2)
		end
	},
	{
		name = "UseAction",
		callback = function()
			UseAction(2)
		end
	},
	{
		name = "UseContainerItem",
		callback = function()
			UseContainerItem(0, 1)
		end
	},
	
	{
		name = "AssistUnit",
		callback = function()
			AssistUnit("player")
		end
	},
	{
		name = "AttackTarget",
		callback = function()
			AttackTarget()
		end
	},
	{
		name = "TargetLastEnemy",
		callback = function()
			TargetLastEnemy()
		end
	},
	{
		name = "TargetLastTarget",
		callback = function()
			TargetLastTarget()
		end
	},
	{
		name = "TargetNearestFriend",
		callback = function()
			TargetNearestFriend()
		end
	},
	{
		name = "TargetNearestEnemy",
		callback = function()
			TargetNearestEnemy()
		end
	},
	{
		name = "TargetUnit",
		callback = function()
			TargetUnit("player")
		end
	},
	{
		name = "ClearTarget",
		callback = function()
			ClearTarget()
		end
	},
	{
		name = "PetAttack",
		callback = function()
			PetAttack()
		end
	},
	{
		name = "CameraOrSelectOrMoveStart",
		callback = function()
			CameraOrSelectOrMoveStart()
		end
	},
	{
		name = "CameraOrSelectOrMoveStop",
		callback = function()
			CameraOrSelectOrMoveStop()
		end
	},
	{
		name = "TurnOrActionStart",
		callback = function()
			TurnOrActionStart()
		end
	},
	
	{
		name = "CancelShapeshiftForm",
		callback = function()
			CancelShapeshiftForm()
		end
	},
	{
		name = "CastShapeShiftForm",
		callback = function()
			CastShapeshiftForm(1)
		end
	},
	{
		name = "CastSpell",
		callback = function()
			CastSpell(1, "spell")
		end
	},
	{
		name = "CastSpellByName",
		callback = function()
			CastSpellByName("Wrath")
		end
	},
	{
		name = "SpellStopCasting",
		callback = function()
			SpellStopCasting()
		end
	},
	{
		name = "SpellStopTargeting",
		callback = function()
			SpellStopTargeting()
		end
	},
	{
		name = "SpellTargetUnit",
		callback = function()
			SpellTargetUnit("player")
		end
	},
	
	{
		name = "MoveBackwardStart",
		callback = function()
			MoveBackwardStart(GetTime())
		end
	},
	{
		name = "MoveBackwardStop",
		callback = function()
			MoveBackwardStop(GetTime())
		end
	},
	{
		name = "MoveForwardStart",
		callback = function()
			MoveForwardStart(GetTime())
		end
	},
	{
		name = "MoveForwardStop",
		callback = function()
			MoveForwardStop(GetTime())
		end
	},
	{
		name = "StrafeLeftStart",
		callback = function()
			StrafeLeftStart(GetTime())
		end
	},
	{
		name = "StrafeLeftStop",
		callback = function()
			StrafeLeftStop(GetTime())
		end
	},
	{
		name = "StrafeRightStart",
		callback = function()
			StrafeRightStart(GetTime())
		end
	},
	{
		name = "StrafeRightStop",
		callback = function()
			StrafeRightStop(GetTime())
		end
	},
	{
		name = "ToggleAutoRun",
		callback = function()
			ToggleAutoRun()
		end
	},
	{
		name = "TurnLeftStart",
		callback = function()
			TurnLeftStart(GetTime())
		end
	},
	{
		name = "TurnLeftStop",
		callback = function()
			TurnLeftStop(GetTime())
		end
	},
	{
		name = "TurnRightStart",
		callback = function()
			TurnRightStart(GetTime())
		end
	},
	{
		name = "TurnRightStop",
		callback = function()
			TurnRightStop(GetTime())
		end
	},
	{
		name = "SitStandOrDescendStart",
		callback = function()
			SitStandOrDescendStart(GetTime())
		end
	},
	{
		name = "SitOrStand",
		callback = function()
			SitOrStand()
		end
	},
	
	{
		name = "RunMacro",
		callback = function()
			RunMacro(1)
		end
	},
	{
		name = "RunMacroText",
		callback = function()
			RunMacroText("/yell hello")
		end
	},
	
	{
		name = "GuildUninvite",
		callback = function()
			GuildUninvite("foobarzebra")
		end
	},
	{
		name = "UninviteUnit",
		callback = function()
			UninviteUnit("foobarzebra")
		end
	},
	
	{
		name = "Stuck",
		callback = function()
			Stuck()
		end
	},
	{
		name = "ReplaceEnchant",
		callback = function()
			ReplaceEnchant() -- TODO
		end
	},
	{
		name = "ReloadUI",
		callback = function()
			ReloadUI()
		end
	},
	{
		name = "SetCurrentTitle",
		callback = function()
			SetCurrentTitle(1)
		end
	},
	{
		name = "StartDuel",
		callback = function()
			StartDuel("foobarzebra")
		end
	},
	
	{
		name = "Jump",
		callback = function()
			Jump()
		end
	},
	{
		name = "JumpOrAscendStart",
		callback = function()
			JumpOrAscendStart()
		end
	},
	{
		name = "AscendStop",
		callback = function()
			AscendStop()
		end
	},
	{
		name = "CancelUnitBuff",
		callback = function()
			CancelUnitBuff("player", "Rejuvination")
		end
	},
	{
		name = "ClearFocus",
		callback = function()
			ClearFocus()
		end
	},
	{
		name = "DescendStop",
		callback = function()
			DescendStop()
		end
	},
	{
		name = "DestroyTotem",
		callback = function()
			DestroyTotem(1)
		end
	},
	{
		name = "FocusUnit",
		callback = function()
			FocusUnit("player")
		end
	},
	{
		name = "PitchDownStart",
		callback = function()
			PitchDownStart()
		end
	},
	{
		name = "PitchDownStop",
		callback = function()
			PitchDownStop()
		end
	},
	{
		name = "InteractUnit",
		callback = function()
			InteractUnit("target")
		end
	},
	{
		name = "ClearPartyAssignment",
		callback = function()
			ClearPartyAssignment()
		end
	},
	{
		name = "DisableSpellAutocast",
		callback = function()
			DisableSpellAutocast("Wrath")
		end
	},
	{
		name = "EnableSpellAutocast",
		callback = function()
			EnableSpellAutocast("Wrath")
		end
	},
	{
		name = "GuildInvite",
		callback = function()
			GuildInvite("target")
		end
	},
	{
		name = "MoveAndSteerStart",
		callback = function()
			MoveAndSteerStart()
		end
	},
	{
		name = "MoveAndSteerStop",
		callback = function()
			MoveAndSteerStop()
		end
	},
	{
		name = "NewGMTicket",
		callback = function()
			NewGMTicket(1, "I am stuck")
		end
	},
	{
		name = "PetAggressiveMode",
		callback = function()
			PetAggressiveMode()
		end
	},
	{
		name = "PetAbandon",
		callback = function()
			PetAbandon()
		end
	},
	{
		name = "PetFollow",
		callback = function()
			PetFollow("player")
		end
	},
	{
		name = "PetMoveTo",
		callback = function()
			PetMoveTo(0.5, 0.5)
		end
	},
	{
		name = "PetPassiveMode",
		callback = function()
			PetPassiveMode()
		end
	},
	{
		name = "PetStopAttack",
		callback = function()
			PetStopAttack()
		end
	},
	{
		name = "PetWait",
		callback = function()
			PetWait()
		end
	},
	{
		name = "PlaceAuctionBid",
		callback = function()
			PlaceAuctionBid()
		end
	},
	{
		name = "PurchaseSlot",
		callback = function()
			PurchaseSlot()
		end
	},
	{
		name = "SetTradeMoney",
		callback = function()
			SetTradeMoney(100)
		end
	},
	{
		name = "StopAttack",
		callback = function()
			StopAttack()
		end
	},
	{
		name = "StopMacro",
		callback = function()
			StopMacro()
		end
	},
	{
		name = "TargetNearestEnemyPlayer",
		callback = function()
			TargetNearestEnemyPlayer()
		end,
	},
	{
		name = "TargetNearestFriendPlayer",
		callback = function()
			TargetNearestFriendPlayer()
		end
	},
	{
		name = "TargetNearestPartyMember",
		callback = function()
			TargetNearestPartyMember()
		end
	},
	{
		name = "TargetNearestRaidMember",
		callback = function()
			TargetNearestRaidMember()
		end
	},
	{
		name = "TargetTotem",
		callback = function()
			TargetTotem(1)
		end
	},
	{
		name = "TogglePetAutocast",
		callback = function()
			TogglePetAutocast(1)
		end
	}
}

function create_button(desc, parent) 
	local button = CreateFrame("Button", "action", parent)
	button:SetPoint("CENTER", parent, "TOPLEFT", desc.x, desc.y)
	button:SetWidth(button_width)
	button:SetHeight(button_height)

	button:SetText(desc.name)
	if button.SetNormalFontObject then
		button:SetNormalFontObject("GameFontNormalSmall")
	else
		button:SetTextFontObject("GameFontNormalSmall")
	end

	

	button:SetNormalTexture("Interface/Buttons/UI-Panel-Button-Up")
	button:SetHighlightTexture("Interface/Buttons/UI-Panel-Button-Highlight")
	button:SetPushedTexture("Interface/Buttons/UI-Panel-Button-Down")

	button:RegisterForClicks("LEFTBUTTONUP")

	button:SetScript("OnClick", function()
		if(desc.callback) then
			desc.callback()
		end
	end)
	
	button:Show()
end



local current_x = start_point_x
local current_y = start_point_y
for k,v in ipairs(api_descriptions) do 
	v.x = current_x
	v.y = current_y
	create_button(v, parent)
	current_y = current_y - step_y
	if(-current_y > max_y) then
		current_y = start_point_y
		current_x = current_x + step_x
	end
end
